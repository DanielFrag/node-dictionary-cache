const RandomPolicyCache = (() => {
  const privateData = new WeakMap();
  class RandomPolicyCache {
    constructor(sizeLimit = 2) {
      if (typeof sizeLimit !== 'number' || sizeLimit !== parseInt(sizeLimit)) {
        throw new RangeError (`The cache's size limit must be an integer`);
      }
      if (sizeLimit < 1) {
        throw new RangeError (`The cache's size limit must be greater than 1`);
      }
      privateData.set(this, {
        map: new Map(),
        sizeLimit
      });
    }
    get(key) {
      return privateData.get(this).map.get(key);
    }
    keys() {
      return Array.from(privateData.get(this).map.keys());
    }
    set(key, value) {
      const { map, sizeLimit} = privateData.get(this);
      if (!map.has(key) && sizeLimit < map.size + 1) {
        const keyIndexToRemove = Math.floor(Math.random() * sizeLimit);
        const keyToRemove = Array.from(map.keys())[keyIndexToRemove];
        map.delete(keyToRemove);
      }
      map.set(key, value);
      return;
    }
  }
  return RandomPolicyCache;
})();
export default RandomPolicyCache;
