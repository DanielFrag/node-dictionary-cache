export default class TreeNode {
  constructor(parentNode) {
    this._dataPathsArray = ['_leftNode', '_rightNode'];
    this._parentNode = parentNode;
    this._pointedData = 0;
    this._leftNode = null;
    this._rightNode = null;
  }
  changePointer() {
    this._pointedData = (this._pointedData + 1) % 2;
  }
  changePointerChain(caller) {
    if (this.getChild() === caller) {
      this.changePointer();
    }
    if (this._parentNode) {
      this._parentNode.changePointerChain(this);
    }
  }
  getChild() {
    return this[this._dataPathsArray[this._pointedData]];
  }
  getNextLeaf() {
    const nextNode = this.getChild();
    if (nextNode.getNextLeaf) {
      return nextNode.getNextLeaf();
    }
    return nextNode;
  }
  setChilds(leftNode, rightNode) {
    this._leftNode = leftNode;
    this._rightNode = rightNode;
  }
}
