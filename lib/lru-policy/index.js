import TreeNode from './tree-node';
import LeafNode from './leaf-node';
const populateNodes = (parentNode, height) => {
  const treeNode = new TreeNode(parentNode); 
  if (height === 1) {
    treeNode.setChilds(new LeafNode(treeNode), new LeafNode(treeNode));
  } else {
    treeNode.setChilds(populateNodes(treeNode, height - 1), populateNodes(treeNode, height - 1));
  }
  return treeNode;
};
const buildMetaTree = (height) => {
  return populateNodes(null, height);
};
const LruPolicyCache = (() => {
  const privateData = new WeakMap();
  class LruPolicyCache {
    constructor(binaryTreeHeight = 1) {
      if (typeof binaryTreeHeight !== 'number' || binaryTreeHeight !== parseInt(binaryTreeHeight)) {
        throw new RangeError (`The cache's binary tree height must be an integer`);
      }
      if (binaryTreeHeight < 1) {
        throw new RangeError (`The cache's binary tree height must contains at least 1 level`);
      }
      privateData.set(this, {
        map: new Map(),
        treeController: buildMetaTree(binaryTreeHeight)
      });
    }
    get(key) {
      const data = privateData.get(this).map.get(key);
      if (!data) {
        return;
      }
      data.relatedLeaf.changePointerChain();
      return data.cachedData;
    }
    keys() {
      return Array.from(privateData.get(this).map.keys());
    }
    set(key, value) {
      const { map, treeController } = privateData.get(this);
      const oldValue = map.get(key);
      if (oldValue) {
        oldValue.relatedLeaf.changePointerChain();
        map.set(key, {
          relatedLeaf: oldValue.relatedLeaf,
          cachedData: value
        });
        return;
      }
      const overrideLeaf = treeController.getNextLeaf();
      const overrideLeafData = overrideLeaf.getData();
      const cachedData = map.get(overrideLeafData);
      if (cachedData && cachedData.relatedLeaf === overrideLeaf) {
        map.delete(overrideLeafData);
      }
      overrideLeaf.setData(key);
      overrideLeaf.changePointerChain();
      map.set(key, {
        relatedLeaf: overrideLeaf,
        cachedData: value
      });
      return;
    }
  }
  return LruPolicyCache;
})();
export default LruPolicyCache;
