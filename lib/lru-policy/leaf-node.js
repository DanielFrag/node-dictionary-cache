export default class LeafNode {
  constructor(parentNode) {
    this._parent = parentNode;
    this._data = undefined;
  }
  changePointerChain() {
    this._parent.changePointerChain(this);
  }
  getData(data) {
    return this._data;
  }
  setData(data) {
    this._data = data;
  }
}
