# cache-memory-dictionary

In-memory dictionary with number of keys limited using a specific replacement policy.

## Synopsis

This cache memory module uses javascript Maps as dictionaries (see the Motivation section). 

These dictionaries have a limited number of keys. When the limit are reached, some other previously existing key is removed to make room for the new key (according to a specific replacement policy). The type of keys can be string, symbol, function, object, and any primitive.

The module exposes a factory that can build a dictionary with the requested replacement policy.

OBS: This project uses ES6 features. So the Node version must be >= 6.14.3

#### LRU (last recently used)

When the dictionary limit is reached, the last recently used key is replaced.

#### Random

When the dictionary limit is reached, an existing key is choiced randomly to be replaced.

## Code Example

- Instantiate a dictionary with lru replacement policy, limited in 2 entries.

OBS: The lru policy dictionary limited in 2 keys is returned by default.
```
const cacheDictionary = require('cache-memory-dictionary');
const lruCache = cacheDictionary();

const bar = function() {};
const foo = 'foo';
const sunda = 1;

/* set key-value pairs */
lruCache.set(bar, 'bar');
lruCache.set(foo, {
  foo: 1
});

/* use a key calling the get/set method */
lruCache.get(bar);

/* replace the last recently used key: foo */
lruCache.set(sunda, 'sunda');

/* get values */
lruCache.get('foo'); /* undefined */
lruCache.get(bar); /* 'bar' */
lruCache.get(sunda); /* 'sunda' */

/* get active keys */
lruCache.keys(); /* [ [Function: bar], 1 ] */
```
- Instantiate a dictionary with lru replacement policy, limited in 8 entries.

OBS: The lru policy uses a binary tree to handle the keys replacement. The "sizeReference" corresponds to the height of this tree.
```
const cacheDictionary = require('cache-memory-dictionary');
const lruCache = cacheDictionary('lru', 3); /* 8 entries = 2^3 */

/* set values */
for (let i = 0; i < 8; i++) {
  lruCache.set(i.toString(), i);
}

/* active keys */
lruCache.keys(); /* ['0', '1', '2', ... '7'] */

/* use a key */
lruCache.get('4'); /* lru => '4' */

/* set new values */
for (let i = 8; i < 15; i++) {
  lruCache.set(i.toString(), i);
}

/* active keys */
lruCache.keys(); /* ['4', '8', '9', ... '14'] */
```
- Instantiate a dictionary with random replacement policy, limited in 3 entries.
```
const cacheDictionary = require('cache-memory-dictionary');
const lruCache = cacheDictionary('random', 3);

/* set keys */
for (let i = 0; i < 9; i++) {
  lruCache.set(i, i.toString());
}

/* recover only 3 keys */
let hit = 0;
for (let i = 0; i < 9; i++) {
  if (lruCache.get(i)) {
    hit++;
  }
}
console.log(hit); /* 3 */
```

## Motivation

The reasons to use Maps insted javascript objects are listed [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map), on section "Objects and maps compared". Besides that, this module also keeps the number of keys limited.

## Tests

- With report
```
npm test
```
- Single tests
```
npm run mocha
```

## License
MIT
