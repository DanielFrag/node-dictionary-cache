import LruPolicyCache from './lib/lru-policy';
import RandomPolicyCache from './lib/random-policy';
const cacheOptions = {
	lru: LruPolicyCache,
	random: RandomPolicyCache
}
/**
 * @param { String } replacementPolicy lru or random
 * @param { Number } sizeReference an integer used to limit the number of keys
 */
const cacheFactory = (replacementPolicy, sizeReference) => {
	if (typeof replacementPolicy === 'string' && cacheOptions[replacementPolicy]) {
		return new cacheOptions[replacementPolicy](sizeReference);
	}
	return new LruPolicyCache();
}
export default cacheFactory;
