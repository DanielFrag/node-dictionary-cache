import { assert, expect } from 'chai';
import dataDriven from 'data-driven';
import LruPolicyCache from '../../../lib/lru-policy';
import RandomPolicyCache from '../../../lib/random-policy';
const cahceModules = [{
  name: 'LruPolicyCache',
  class: LruPolicyCache
}, {
  name: 'RandomPolicyCache',
  class: RandomPolicyCache
}];
describe('Cache modules intance tests', () => {
  dataDriven(cahceModules, () => {
    it('{name} instance', (ctx) => {
      const CacheClass = ctx.class;
      const cacheInstance = new CacheClass();
      assert.exists(cacheInstance);
      assert.exists(cacheInstance.get);
      assert.typeOf(cacheInstance.get, 'function');
      assert.exists(cacheInstance.set);
      assert.typeOf(cacheInstance.set, 'function');
    });
  });
  dataDriven(cahceModules, () => {
    it('{name} interface', (ctx) => {
      const CacheClass = ctx.class;
      const cacheInstance = new CacheClass();
      const cacheInstanceKeys = Object.keys(cacheInstance);
      const cacheInstanceMethods = Object.getOwnPropertyNames(cacheInstance.constructor.prototype);
      assert.strictEqual(cacheInstanceKeys.length, 0);
      assert.strictEqual(cacheInstanceMethods.length, 4);
      assert.deepEqual(cacheInstanceMethods, ['constructor', 'get', 'keys', 'set']);
    });
  });
  dataDriven(cahceModules, () => {
    it('{name} empty', (ctx) => {
      const CacheClass = ctx.class;
      const cacheInstance = new CacheClass();
      assert.deepEqual(cacheInstance.keys(), []);
    });
  });
  dataDriven(cahceModules, () => {
    it('{name} should allow falsy values as keys', (ctx) => {
      const CacheClass = ctx.class;
      const cacheInstance = new CacheClass(5);
      cacheInstance.set(undefined, 'undefined');
      cacheInstance.set(null, 'null');
      cacheInstance.set(false, 'false');
      cacheInstance.set(0, '0');
      cacheInstance.set(NaN, 'NaN');
      assert.strictEqual(cacheInstance.get(undefined), 'undefined');
      assert.strictEqual(cacheInstance.get(null), 'null');
      assert.strictEqual(cacheInstance.get(0), '0');
      assert.strictEqual(cacheInstance.get(false), 'false');
      assert.strictEqual(cacheInstance.get(NaN), 'NaN');
      assert.deepEqual(cacheInstance.keys(), [undefined, null, false, 0, NaN]);
    });
  });
});