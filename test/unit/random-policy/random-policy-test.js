import { assert, expect } from 'chai';
import dataDriven from 'data-driven';
import { stub } from 'sinon';
import RandomPolicyCache from '../../../lib/random-policy';
describe('random-policy module: unit test', () => {
  it('Should throw an error if the specified limit leather than 1', () => {
    expect(() => new RandomPolicyCache(0))
      .to
      .throw(`The cache's size limit must be greater than 1`);
  });
  it('Should throw an error if the specified limit param is not a number', () => {
    expect(() => new RandomPolicyCache({}))
      .to
      .throw(`The cache's size limit must be an integer`);
  });
  it('Should keep the cache limit of entries', () => {
    const limit = 3;
    const rpCache = new RandomPolicyCache(limit);
    rpCache.set(0, '0');
    rpCache.set(1, '1');
    rpCache.set(2, '2');
    rpCache.set(3, '3');
    rpCache.set(4, '4');
    rpCache.set(5, '5');
    assert.isTrue(Array.isArray(rpCache.keys()));
    assert.strictEqual(rpCache.keys().length, 3);
  });
  it('Should update the value for a key', () => {
    const limit = 2;
    const rpCache = new RandomPolicyCache(limit);
    rpCache.set(0, '0');
    rpCache.set(0, '1');
    rpCache.set(0, '2');
    rpCache.set(0, '3');
    assert.strictEqual(rpCache.keys().length, 1);
    assert.strictEqual(rpCache.get(0), '3');
  });
  describe('Should keep the cache limit of entries, deleting old entries randomly', () => {
    dataDriven([{
      index: 0,
      randomResult: 0.1
    }, {
      index: 1,
      randomResult: .2
    }, {
      index: 2,
      randomResult: .4
    }, {
      index: 3,
      randomResult: .6
    }, {
      index: 4,
      randomResult: .9999
    }], () => {
      it('Math.random stub to return {randomResult}', (ctx) => {
        const mathStub = stub(Math, 'random').returns(ctx.randomResult);
        const limit = 5;
        const rpCache = new RandomPolicyCache(limit);
        rpCache.set(0, '0');
        rpCache.set(1, '1');
        rpCache.set(2, '2');
        rpCache.set(3, '3');
        rpCache.set(4, '4');
        rpCache.set(5, '5');
        mathStub.restore();
        assert.notExists(rpCache.get(ctx.index));
      });
    });
  });
});