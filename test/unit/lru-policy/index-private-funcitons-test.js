import { assert } from 'chai';
import dataDriven from 'data-driven';
import rewire from 'rewire';
import TreeNode from '../../../lib/lru-policy/tree-node';
import LeafNode from '../../../lib/lru-policy/leaf-node';
const lruPolicyModule = rewire('../../../lib/lru-policy');
const populateNodes = lruPolicyModule.__get__('populateNodes');
const buildMetaTree = lruPolicyModule.__get__('buildMetaTree');
describe('lru-policy unit tests on private functions', () => {
  it('Should populate one tree node with two leaf nodes as childs', () => {
    const result = populateNodes(null, 1);
    const treeNode = new TreeNode(null);
    treeNode.setChilds(new LeafNode(treeNode), new LeafNode(treeNode));
    assert.deepEqual(result, treeNode);
    assert.instanceOf(result, TreeNode);
  });
  describe('Should build a tree node with height equals to:', () => {
    dataDriven([{
      height: 3
    }, {
      height: 4
    }, {
      height: 5
    }, {
      height: 6
    }, {
      height: 7
    }, {
      height: 8
    },], () => {
      it('{height}', (ctx) => {
        const countLeaves = (node) => {
          if (!node instanceof TreeNode) {
            throw new Error('Invalid node');
          }
          if (node._leftNode instanceof LeafNode && node._rightNode instanceof LeafNode) {
            return 2;
          }
          return countLeaves(node._leftNode) + countLeaves(node._rightNode);
        };
        const countHeight = (node, currentHeight) => {
          if (!node instanceof TreeNode) {
            throw new Error('Invalid node');
          }
          if (node._leftNode instanceof LeafNode && node._rightNode instanceof LeafNode) {
            return currentHeight;
          }
          const leftHeight = countHeight(node._leftNode, currentHeight + 1);
          const rightHeight = countHeight(node._rightNode, currentHeight + 1)
          assert.isTrue(leftHeight === rightHeight);
          return leftHeight;
        };
        const result = buildMetaTree(ctx.height);
        assert.instanceOf(result, TreeNode);
        assert.strictEqual(countHeight(result, 1), ctx.height);
        assert.strictEqual(countLeaves(result), Math.pow(2, ctx.height));
      });
    });
  });
});
