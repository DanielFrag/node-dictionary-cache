import { assert, expect } from 'chai';
import LruPolicyCache from '../../../lib/lru-policy';
describe('lru-policy unit tests', () => {
  it('Should throw an error if the specified tree height leather than 1', () => {
    expect(() => new LruPolicyCache(0))
      .to
      .throw(`The cache's binary tree height must contains at least 1 level`);
  });
  it('Should throw an error if the specified limit param is not a number', () => {
    expect(() => new LruPolicyCache({}))
      .to
      .throw(`The cache's binary tree height must be an integer`);
  });
  it('Should keep the cache limit', () => {
    const limit = 3;
    const entriesReference = Math.pow(2, limit);
    const lruCache = new LruPolicyCache(limit);
    for (let i = 0; i <= entriesReference; i++) {
      lruCache.set(i.toString(), i);
    }
    assert.isTrue(Array.isArray(lruCache.keys()));
    assert.strictEqual(lruCache.keys().length, entriesReference);
  });
  it('Should override the oldest key based on the cache limit', () => {
    const limit = 3;
    const entriesReference = Math.pow(2, limit);
    const lruCache = new LruPolicyCache(limit);
    for (let i = 0; i <= entriesReference; i++) {
      lruCache.set(i.toString(), i);
    }
    assert.notExists(lruCache.get('0'));
    assert.exists(lruCache.get(entriesReference.toString()));
    assert.strictEqual(lruCache.get(entriesReference.toString()), entriesReference);
  });
  it('Should update a value related to an existing key without change any other key value pair', () => {
    const limit = 3;
    const entriesReference = Math.pow(2, limit);
    const lruCache = new LruPolicyCache(limit);
    for (let i = 0; i < entriesReference; i++) {
      lruCache.set(i.toString(), i);
    }
    const keyToUpdate = parseInt(entriesReference / 2).toString();
    lruCache.set(keyToUpdate, 'updated');
    for (let i = 0; i < entriesReference; i++) {
      const key = i.toString();
      const data = lruCache.get(key);
      if (key === keyToUpdate) {
        assert.strictEqual(data, 'updated');
      } else {
        assert.strictEqual(data, i);
      }
    }
  });
  it('Should convert a key into the newest key when it is requested', () => {
    const limit = 3;
    const entriesReference = Math.pow(2, limit);
    const lruCache = new LruPolicyCache(limit);
    const expectedKeys = [];
    for (let i = 0; i < entriesReference; i++) {
      lruCache.set(i.toString(), i);
    }
    const valueToKeep = parseInt(entriesReference / 2);
    const keyToConvert = valueToKeep.toString();
    lruCache.get(keyToConvert);
    expectedKeys.push(keyToConvert);
    for (let i = entriesReference; i < (2 * entriesReference) - 1; i++) {
      const k = i.toString();
      lruCache.set(k, i);
      expectedKeys.push(k);
    }
    assert.exists(lruCache.get(keyToConvert));
    assert.strictEqual(lruCache.get(keyToConvert), valueToKeep);
    assert.deepEqual(lruCache.keys(), expectedKeys);
  });
  it('Should convert the oldest key into the newest key when it is requested', () => {
    const limit = 3;
    const entriesReference = Math.pow(2, limit);
    const lruCache = new LruPolicyCache(limit);
    const expectedKeys = [];
    for (let i = 0; i < entriesReference; i++) {
      lruCache.set(i.toString(), i);
    }
    lruCache.get('0');
    expectedKeys.push('0');
    for (let i = entriesReference; i < (2 * entriesReference) - 1; i++) {
      const k = i.toString();
      lruCache.set(k, i);
      expectedKeys.push(k);
    }
    assert.exists(lruCache.get('0'));
    assert.strictEqual(lruCache.get('0'), 0);
    assert.deepEqual(lruCache.keys(), expectedKeys);
  });
});
