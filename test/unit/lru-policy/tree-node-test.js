import { assert } from 'chai';
import dataDriven from 'data-driven';
import TreeNode from '../../../lib/lru-policy/tree-node';
const buildTestTree = () => {
  const root = new TreeNode(null);
  const firstHLeftNode = new TreeNode(root);
  const firstHRightNode = new TreeNode(root);
  root.setChilds(firstHLeftNode, firstHRightNode);
  const secondHLeftNode = new TreeNode(firstHLeftNode);
  const secondHLeftMiddleNode = new TreeNode(firstHLeftNode);
  firstHLeftNode.setChilds(secondHLeftNode, secondHLeftMiddleNode);
  const secondHMiddleRightNode = new TreeNode(firstHRightNode);
  const secondHRightNode = new TreeNode(firstHRightNode);
  firstHRightNode.setChilds(secondHMiddleRightNode, secondHRightNode);
  const changeTreeNodePointer = (treeNode, leaf) => {
    treeNode.changePointerChain(leaf);
  };
  const leaf1 = {
    name: 'leaf1',
    callChangePointerChain() {
      changeTreeNodePointer(secondHLeftNode, this);
    }
  };
  const leaf2 = {
    name: 'leaf2',
    callChangePointerChain() {
      changeTreeNodePointer(secondHLeftNode, this);
    }
  };
  const leaf3 = {
    name: 'leaf3',
    callChangePointerChain() {
      changeTreeNodePointer(secondHLeftMiddleNode, this);
    }
  };
  const leaf4 = {
    name: 'leaf4',
    callChangePointerChain() {
      changeTreeNodePointer(secondHLeftMiddleNode, this);
    }
  };
  const leaf5 = {
    name: 'leaf5',
    callChangePointerChain() {
      changeTreeNodePointer(secondHMiddleRightNode, this);
    }
  };
  const leaf6 = {
    name: 'leaf6',
    callChangePointerChain() {
      changeTreeNodePointer(secondHMiddleRightNode, this);
    }
  };
  const leaf7 = {
    name: 'leaf7',
    callChangePointerChain() {
      changeTreeNodePointer(secondHRightNode, this);
    }
  };
  const leaf8 = {
    name: 'leaf8',
    callChangePointerChain() {
      changeTreeNodePointer(secondHRightNode, this);
    }
  };
  secondHLeftNode.setChilds(leaf1, leaf2);
  secondHLeftMiddleNode.setChilds(leaf3, leaf4);
  secondHMiddleRightNode.setChilds(leaf5, leaf6);
  secondHRightNode.setChilds(leaf7, leaf8);
  return {
    tree: root,
    firstH: [firstHLeftNode, firstHRightNode],
    secondH: [secondHLeftNode, secondHLeftMiddleNode, secondHMiddleRightNode, secondHRightNode],
    leaves: [leaf1, leaf2, leaf3, leaf4, leaf5, leaf6, leaf7, leaf8]
  }
};
describe('TreeNode class unit tests', () => {
  it('Should return the current pointed child', () => {
    const treeNode = new TreeNode(null);
    treeNode.setChilds('foo', 'bar');
    const pointedChild = treeNode.getChild();
    assert.exists(pointedChild);
    assert.strictEqual(treeNode.getChild(), pointedChild);
  });
  it('Should change the pointed child', () => {
    const treeNode = new TreeNode(null);
    treeNode.setChilds('foo', 'bar');
    const oldPointedChild = treeNode.getChild();
    treeNode.changePointer();
    assert.notStrictEqual(treeNode.getChild(), oldPointedChild);
  });
  it('Should get the next leaf', () => {
    const root = new TreeNode(null);
    const leftNode = new TreeNode(root);
    const rightNode = new TreeNode(root);
    const firstLeaf = {
      node: 'left',
      position: 'left'
    };
    const secondLeaf = {
      node: 'left',
      position: 'right'
    };
    const thirdtLeaf = {
      node: 'right',
      position: 'left'
    };
    const fourthLeaf = {
      node: 'right',
      position: 'right'
    };
    leftNode.setChilds(firstLeaf, secondLeaf);
    rightNode.setChilds(thirdtLeaf, fourthLeaf);
    root.setChilds(leftNode, rightNode);
    assert.deepEqual(root.getNextLeaf(), firstLeaf);
    leftNode.changePointerChain(firstLeaf);
    assert.deepEqual(root.getNextLeaf(), thirdtLeaf);
    rightNode.changePointerChain(thirdtLeaf);
    assert.deepEqual(root.getNextLeaf(), secondLeaf);
    leftNode.changePointerChain(secondLeaf);
    assert.deepEqual(root.getNextLeaf(), fourthLeaf);
    rightNode.changePointerChain(fourthLeaf);
    assert.deepEqual(root.getNextLeaf(), firstLeaf);
  });
  describe('Should simulate the changes on pointer chain and validate it', () => {
    dataDriven([{
      description: 'none',
      leavesToChange: [],
      valueOnRoot: 0,
      mapValuesFirstH: new Map([
        [0, 0],
        [1, 0]
      ]),
      mapValuesSecondH: new Map([
        [0, 0],
        [1, 0],
        [2, 0],
        [3, 0]
      ])
    }, {
      description: 'leaf1',
      leavesToChange: [0],
      valueOnRoot: 1,
      mapValuesFirstH: new Map([
        [0, 1],
        [1, 0]
      ]),
      mapValuesSecondH: new Map([
        [0, 1],
        [1, 0],
        [2, 0],
        [3, 0]
      ])
    }, {
      description: 'leaf1, leaf3',
      leavesToChange: [0, 2],
      valueOnRoot: 1,
      mapValuesFirstH: new Map([
        [0, 0],
        [1, 0]
      ]),
      mapValuesSecondH: new Map([
        [0, 1],
        [1, 1],
        [2, 0],
        [3, 0]
      ])
    }, {
      description: 'leaf1, leaf5, leaf3, leaf7, leaf2, leaf6, leaf4, leaf8',
      leavesToChange: [0, 4, 2, 6, 1, 5, 3, 7],
      valueOnRoot: 0,
      mapValuesFirstH: new Map([
        [0, 0],
        [1, 0]
      ]),
      mapValuesSecondH: new Map([
        [0, 0],
        [1, 0],
        [2, 0],
        [3, 0]
      ])
    }, {
      description: 'leaf1, leaf5, leaf3, leaf7, leaf6',
      leavesToChange: [0, 4, 2, 6, 5],
      valueOnRoot: 0,
      mapValuesFirstH: new Map([
        [0, 0],
        [1, 1]
      ]),
      mapValuesSecondH: new Map([
        [0, 1],
        [1, 1],
        [2, 0],
        [3, 1]
      ])
    }], () => {
      it('Order change: {description}', (ctx) => {
        const treeData = buildTestTree();
        ctx.leavesToChange.forEach((leafIndex) => {
          treeData.leaves[leafIndex].callChangePointerChain();
        });
        assert.strictEqual(treeData.tree._pointedData, ctx.valueOnRoot);
        ctx.mapValuesFirstH.forEach((value, key) => {
          assert.strictEqual(treeData.firstH[key]._pointedData, value);
        });
        ctx.mapValuesSecondH.forEach((value, key) => {
          assert.strictEqual(treeData.secondH[key]._pointedData, value);
        });
      });
    });
  });
});
