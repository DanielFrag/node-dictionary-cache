import { assert } from 'chai';
import LeafNode from '../../../lib/lru-policy/leaf-node';
import TreeNode from '../../../lib/lru-policy/tree-node';
describe('leaf-node unit tests', () => {
  it('Should store a specific data', () => {
    const data = 'sunda';
    const leaf = new LeafNode(null);
    leaf.setData(data);
    assert.strictEqual(leaf.getData(), data);
  });
  it('Should change the pointers on parent', () => {
    const treeNode = new TreeNode();
    const leaf1 = new LeafNode(treeNode);
    const leaf2 = new LeafNode(treeNode);
    const previousPointer = treeNode._pointedData;
    treeNode.setChilds(leaf1, leaf2);
    treeNode.getChild().changePointerChain();
    assert.notStrictEqual(treeNode._pointedData, previousPointer);
  });
});