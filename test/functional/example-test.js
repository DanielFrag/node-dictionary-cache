import { assert } from 'chai';
import cacheDictionary from '../../index';
describe('code used as examples', () => {
  it('first', () => {
    const lruCache = cacheDictionary();
    const bar = function() {};
    const foo = 'foo';
    const sunda = 1;
    /* set key-value pairs */
    lruCache.set(bar, 'bar');
    lruCache.set(foo, {
      foo: 1
    });
    /* use a key calling the get/set method */
    lruCache.get(bar);
    /* replace the oldest key */
    lruCache.set(sunda, 'sunda');
    /* get values */
    lruCache.get('foo'); /* undefined */
    lruCache.get(bar); /* 'bar' */
    lruCache.get(sunda); /* 'sunda' */
    /* get active keys */
    lruCache.keys(); /* [ [Function: bar], 1 ] */
    assert.deepEqual(lruCache.keys(), [ bar, 1 ]);
  });
  it('second', () => {
    const lruCache = cacheDictionary('lru', 3); /* 8 entries = 2^3 */
    /* set values */
    for (let i = 0; i < 8; i++) {
      lruCache.set(i.toString(), i);
    }
    /* active keys */
    lruCache.keys(); /* ['0', '1', '2', ... '7'] */
    assert.deepEqual(lruCache.keys(), ['0', '1', '2', '3', '4', '5', '6', '7']);
    /* use a key */
    lruCache.get('4');
    /* set new values */
    for (let i = 8; i < 15; i++) {
      lruCache.set(i.toString(), i);
    }
    /* active keys */
    lruCache.keys(); /* ['4', '8', '9', ... '14'] */
    assert.deepEqual(lruCache.keys(), ['4', '8', '9', '10', '11', '12', '13', '14']);
  });
  it('third', () => {
    const lruCache = cacheDictionary('random', 3);
    for (let i = 0; i < 9; i++) {
      lruCache.set(i, i.toString());
    }
    let hit = 0;
    for (let i = 0; i < 9; i++) {
      if (lruCache.get(i)) {
        hit++;
      }
    }
    assert.strictEqual(hit, 3);
  });
});
