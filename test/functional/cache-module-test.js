import { assert } from 'chai';
import cacheModule from '../../index';
describe('funcitonal tests', () => {
  it('Should instantiate a default cache (lru)', () => {
    const cache = cacheModule();
    assert.strictEqual(cache.constructor.name, 'LruPolicyCache');
  });
  it('Should instantiate a cache with random policy', () => {
    const cache = cacheModule('random');
    assert.strictEqual(cache.constructor.name, 'RandomPolicyCache');
  });
  it('Should instantiate a cache with lru policy', () => {
    const cache = cacheModule('lru');
    assert.strictEqual(cache.constructor.name, 'LruPolicyCache');
  });
  it('Should instantiate a cache with random policy and size limit equal to 4', () => {
    const cache = cacheModule('random', 4);
    assert.strictEqual(cache.constructor.name, 'RandomPolicyCache');
  });
  it('Should instantiate a cache with lru policy and three height size equal to 4', () => {
    const cache = cacheModule('lru', 4);
    assert.strictEqual(cache.constructor.name, 'LruPolicyCache');
  });
});